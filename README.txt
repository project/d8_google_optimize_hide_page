CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Adds the Google Optimize Hide Page script to your website.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/d8_google_optimize_hide_page

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/d8_google_optimize_hide_page


REQUIREMENTS
------------

This module requires no modules beyond Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * Configure at /admin/config/system/d8_google_optimize_hide_page


MAINTAINERS
-----------

Current maintainers:

 * John Burch http://drupal.org/user/85918
