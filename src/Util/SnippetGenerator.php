<?php

namespace Drupal\d8_google_optimize_hide_page\Util;

/**
 * Implements Snippet Generator class.
 */
class SnippetGenerator {

  /**
   * Generate the JS snippet.
   *
   * @return string
   *   JS snippet.
   */
  public function getSnippet(): string
  {
    $container_ids = d8_google_optimize_hide_page_container_ids();
    $class_name = d8_google_optimize_hide_page_class_name();
    $timeout = d8_google_optimize_hide_page_timeout();

    $container_ids = array_map(
      function ($value) {
        return "'" . trim($value) . "':true";
      },
      $container_ids
    );
    $container_str = implode(',', $container_ids);

    return sprintf(
      "(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
          h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
          (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
          })(window,document.documentElement,'%s','dataLayer',%d,
          {%s});",
      $class_name,
      $timeout,
      $container_str
    );
  }

}
