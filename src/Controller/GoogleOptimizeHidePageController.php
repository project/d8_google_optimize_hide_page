<?php

namespace Drupal\d8_google_optimize_hide_page\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Drupal\d8_google_optimize_hide_page\Util\SnippetGenerator;

/**
 * Implements Google Optimize Hide Page Controller.
 */
class GoogleOptimizeHidePageController extends ControllerBase {

  /**
   * Get JS Snippet.
   *
   * @return Response
   *   JS Snippet.
   */
  public function snippet(): Response
  {
    $snippet = new SnippetGenerator();
    return new Response($snippet->getSnippet(), 200, ['Content-type' => 'application/javascript']);
  }

}
